package com.frank;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
//@MapperScan(basePackages = "com.frank.dao")
public class RecruitBaseServiceApplicationTests {

	@Resource
	private UserMapper userMapper;

	@Test
	public void contextLoads() {
	}


	@Test
	public void testMybatis() {
		UserCriteria userCriteria = new UserCriteria();
		userCriteria.createCriteria().andUserNameEqualTo("frank");

		User user = new User();
		user.setEmail("16503264@163.com");
		userMapper.updateByExampleSelective(user,userCriteria);
		List<User> users = userMapper.selectByExample(userCriteria);
		System.out.println(users);
	}

}
